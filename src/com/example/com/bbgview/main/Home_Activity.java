package com.example.com.bbgview.main;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Home_Activity extends Activity {
	
	private Button selectedLocation;
	private String locationName;
	
	private int unselectedColor = Color.parseColor("#6b7e11");
	private int selectedColor = "#6b7e11".hashCode();
	
	private Button buffalo_loc_button;
	private Button cali_loc_button;
	private Button florida_loc_button;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_layout);
        
        buffalo_loc_button = (Button) findViewById(R.id.buffalo);
        buffalo_loc_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Update(R.id.buffalo);
            	Login();
            }
        });
        
        cali_loc_button = (Button) findViewById(R.id.california);
        cali_loc_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Update(R.id.california);
            	Login();
            }
        });
        
        florida_loc_button = (Button) findViewById(R.id.florida);
        florida_loc_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Update(R.id.florida);
            	Login();

            }
        });
        
        //default location
        selectedLocation = buffalo_loc_button;
        locationName = "Buffalo";
        selectedLocation.setTextColor(selectedColor);
        
    }

    private void Update(int location){
    	selectedLocation.setText(locationName);
    	selectedLocation.setTextColor(unselectedColor);
    	
    	switch(location){
	    	case R.id.buffalo:{
	    		locationName = "Buffalo";
	        	selectedLocation = buffalo_loc_button;
	        	selectedLocation.setText("- Buffalo -");
	    	}break;
	    	case R.id.california:{
	        	locationName = "California";
	        	selectedLocation = cali_loc_button;
	        	selectedLocation.setText("- California -");
	    	} break;
	    	case R.id.florida:{
	        	locationName = "Florida";
	        	selectedLocation = florida_loc_button;
	        	selectedLocation.setText("- Florida -");
	    	}break;
    	}
    	
    	selectedLocation.setTextColor(selectedColor);
    }

    private void Login(){
	    Intent intent = new Intent(this,
	    		Login.class);
	    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    getApplicationContext().startActivity(intent);
    }
}
