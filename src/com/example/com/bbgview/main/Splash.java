package com.example.com.bbgview.main;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;

public class Splash extends Activity{

	MediaPlayer ourSong;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		

		
		Thread timer = new Thread(){
			public void run(){
				try{
					sleep(3000);
				}catch(InterruptedException e){
					e.printStackTrace();
				}finally{
				    Intent intent = new Intent(Splash.this,
					    Home_Activity.class);
				    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				    getApplicationContext().startActivity(intent);

				}
			}
		};
		timer.start();	
	
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		
		super.onPause();
		finish();
	}
	
	

}
