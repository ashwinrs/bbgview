package com.example.com.bbgview.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MenuActivity extends Activity {

    
    Button search,buy,newsfeed,gallery,map,forum;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        search = (Button)findViewById(R.id.Button03);
        buy = (Button)findViewById(R.id.Button02);
        newsfeed = (Button)findViewById(R.id.Button01);
        gallery = (Button)findViewById(R.id.button1);
        map = (Button)findViewById(R.id.Button04);
        forum = (Button)findViewById(R.id.Button05);
        
        search.setOnClickListener(new View.OnClickListener() {

	    @Override
	    public void onClick(View v) {
		Intent intent = new Intent(MenuActivity.this,
			    Search.class);
		    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		    getApplicationContext().startActivity(intent);
	    }
        });
        
        buy.setOnClickListener(new View.OnClickListener() {

	    @Override
	    public void onClick(View v) {
		Intent intent = new Intent(MenuActivity.this,Buy
			    .class);
		    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		    getApplicationContext().startActivity(intent);
	    }
        });
        
        newsfeed.setOnClickListener(new View.OnClickListener() {

	    @Override
	    public void onClick(View v) {
		Intent intent = new Intent(MenuActivity.this,
			    News.class);
		    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		    getApplicationContext().startActivity(intent);
	    }
        });
        
        gallery.setOnClickListener(new View.OnClickListener() {

	    @Override
	    public void onClick(View v) {
		Intent intent = new Intent(MenuActivity.this,
			    Gallery.class);
		    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		    getApplicationContext().startActivity(intent);
	    }
        });
        
        map.setOnClickListener(new View.OnClickListener() {

	    @Override
	    public void onClick(View v) {
		Intent intent = new Intent(MenuActivity.this,
			    Maps.class);
		    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		    getApplicationContext().startActivity(intent);
	    }
        });
        
        forum.setOnClickListener(new View.OnClickListener() {

	    @Override
	    public void onClick(View v) {
		Intent intent = new Intent(MenuActivity.this,
			    Events.class);
		    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		    getApplicationContext().startActivity(intent);
	    }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
}
