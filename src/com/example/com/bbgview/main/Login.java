package com.example.com.bbgview.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Login extends Activity {

    Button login, guest;
    TextView username, password;

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_login);

	login = (Button) findViewById(R.id.button7);
	guest = (Button) findViewById(R.id.button2);

	username = (TextView) findViewById(R.id.editText1);
	password = (TextView) findViewById(R.id.editText2);

	login.setOnClickListener(new View.OnClickListener() {

	    @Override
	    public void onClick(View v) {

		    Intent intent = new Intent(Login.this, MenuActivity.class);
		    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		    getApplicationContext().startActivity(intent);

	    }
	});

	guest.setOnClickListener(new View.OnClickListener() {

	    @Override
	    public void onClick(View v) {
		Intent intent = new Intent(Login.this, MenuActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		getApplicationContext().startActivity(intent);
	    }
	});
    }

    @Override
    protected void onPause() {
	// TODO Auto-generated method stub
	super.onPause();
	finish();

    }
    
    
}
