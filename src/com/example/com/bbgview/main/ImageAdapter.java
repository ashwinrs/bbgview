package com.example.com.bbgview.main;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.com.bbgview.main.R;

public class ImageAdapter extends BaseAdapter {
    private Context mContext;

    public ImageAdapter(Context c) {
	mContext = c;
    }

    @Override
    public int getCount() {
	return mThumbIds.length;
    }

    @Override
    public Object getItem(int position) {
	return mThumbIds[position];
    }

    @Override
    public long getItemId(int position) {
	return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	// TODO Auto-generated method stub
	ImageView imageView;
	if (convertView == null) { // if it's not recycled, initialize some
				   // attributes
	    imageView = new ImageView(mContext);
	    imageView.setLayoutParams(new GridView.LayoutParams(180,180));
	    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
	    imageView.setPadding(8, 8, 8, 8);
	} else {
	    imageView = (ImageView) convertView;
	}

	imageView.setImageResource(mThumbIds[position]);
	return imageView;
    }

    // references to our images
    Integer[] mThumbIds = { R.drawable.gallery_1, R.drawable.gallery_2,
	    R.drawable.gallery_3, R.drawable.gallery_4, R.drawable.gallery_5,
	    R.drawable.gallery_6, R.drawable.gallery_7, R.drawable.gallery_8,
	    R.drawable.gallery_9, R.drawable.gallery_10, R.drawable.gallery_11,
	    R.drawable.gallery_12, R.drawable.gallery_13,
	    R.drawable.gallery_14, R.drawable.gallery_15, R.drawable.gallery_1,
	    R.drawable.gallery_2, R.drawable.gallery_3, R.drawable.gallery_4,
	    R.drawable.gallery_5, R.drawable.gallery_6, R.drawable.gallery_7,
	    R.drawable.gallery_8, R.drawable.gallery_9, R.drawable.gallery_10,
	    R.drawable.gallery_11, R.drawable.gallery_12,
	    R.drawable.gallery_13, R.drawable.gallery_14, R.drawable.gallery_15 };

}
