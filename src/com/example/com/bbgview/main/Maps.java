package com.example.com.bbgview.main;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ImageView;

public class Maps extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        
        ImageView imgView=(ImageView) findViewById(R.id.imageView1);
        Drawable  drawable  = getResources().getDrawable(R.drawable.bbg_map);
        imgView.setImageDrawable(drawable);
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_maps, menu);
        return true;
    }
}
