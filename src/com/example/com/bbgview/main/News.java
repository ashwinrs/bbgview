package com.example.com.bbgview.main;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class News extends ListActivity {

    static final String[] News_Elements = new String[] {
	    "Arcangel Gallery - Autistic Services Exhibit Video\n\nhttp://www.youtube.com/watch?v=wsb5Sf2qYrc",
	    "Take our Orchid Show Survey!\n\nhttp://survey.constantcontact.com/survey/a07e6irftvhh8ejsxna/start",
	    "October Medicinal Garden Feature\n\n This month is Breast Cancer Awareness & Flu Prevention at the Gardens!",
	    "Orchid Show October 13-14 2012\n\n The Orchid Show was a huge success and thank you to everyone who came and supported the show! Check out the video below to see the beautiful orchids that were here at the Gardens!" };

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_news,
		News_Elements));

	ListView listView = getListView();
	listView.setTextFilterEnabled(true);

	listView.setOnItemClickListener(new OnItemClickListener() {
	    public void onItemClick(AdapterView<?> parent, View view,
		    int position, long id) {
		// When clicked, show a toast with the TextView text
		Toast.makeText(getApplicationContext(),
			((TextView) view).getText(), Toast.LENGTH_SHORT).show();
	    }
	});

    }

}
