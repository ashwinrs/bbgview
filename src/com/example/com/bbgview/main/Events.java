package com.example.com.bbgview.main;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Events extends ListActivity {


    static final String[] Events_Elements = new String[] { "PHOTO CONTEST\nGarden Members - First entry free, each additional entry $6, Non-Members - Each entry $8",
	"YOGA AT THE GARDENS\nGentle hatha yoga, designed for all ages and fitness levels. Led by Leanne Oldenbrook from Crescent Moon Yoga",
	"AUTISM SERVICES\nOctober 13 - November 18",
	"CHRYSANTHEMUM SHOW\nOctober 20 - November 11",
	"POINSETTIA SHOW\nNovember 23 - December 30",
	"GARDEN RAILWAY ECHIBIT\nNovember 23 - December 30",
	 };
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_events,Events_Elements));
	 
	ListView listView = getListView();
	listView.setTextFilterEnabled(true);

	listView.setOnItemClickListener(new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent, View view,
				int position, long id) {
		    // When clicked, show a toast with the TextView text
		    Toast.makeText(getApplicationContext(),
			((TextView) view).getText(), Toast.LENGTH_SHORT).show();
		}
	});        
        
    }


}
