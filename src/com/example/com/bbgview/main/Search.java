package com.example.com.bbgview.main;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class Search extends Activity implements OnItemSelectedListener {

    TextView display;
    String[] search_words = { "Silver Bismarkia", "Teddy Bear Palm",
	    "cashew tree", "Lychee Fruit", "Dragon Fruit", "Pitaya",
	    "Chocolate Tree", "Pitch Apple", "Clusia Major", "Manikara zapota",
	    "Sapodilla", "The Chewing Gum Tree", "Mimosa pudica",
	    "Sensitive Plant" };

    String[] search_values = {

	    "Bismarkia Silver Select or Silver Bismarkia\n\nFamily:  Palmae; Native to Tropical Regions\n\nOutstanding in its striking silver color, this native of Madagascar makes a very dramatic landscape plant. This tough plant is drought resistant but grows best with regular and adequate moisture.",
	    "Dypsis leptocheilos or Teddy Bear Palm\n\nFamily:  Palmae; Native to Tropical Regions\n\nThis palm is named Teddy Bear Palm because the leaf crown has an outstanding color of deep orange-brown and is felt-like to the touch.",
	    "Anacardium occidentale or cashew tree\n\nFamily:  Anacardiaceae; Native to the West Indies\n\nRoast them and toast them. The true fruit of the cashew is the kidney shaped drupe  (a single seed as in a peach) that grows at the end of the cashew apple.  But watch out! The seed is surrounded by a double shell containing the potent skin irritant, urushiol.  Properly roasting the cashew destroys the toxin, but this must be done out doors as the smoke can cause severe life-threatening reactions if inhaled",
	    "Litchi chinensis or the Lychee Fruit\n\nFamily:  Sapindaceae; Native to South China\n\nHmm Chanel or a dab of lychee fruit behind each ear?  This fresh fruit has a delicate, whitish pulp with a perfume flavor.  Rich in vitamin C, on average nine lychee fruits would meet an adult�s daily recommended Vitamin C requirement. ",
	    "Hylocereus sp. or The Dragon Fruit or Pitaya or Pitahaya\n\nFamily:  Cactaceae; Native to Mexico, Central America and South America\nA sheep in wolf's clothing?  This scary looking fruit of the Hylocereus species has a taste that�s been described as being very bland like a melon or kiwi with a mild sweetness.  Its black crunchy seeds are eaten together with the flesh but don't eat the skin. The skins of most commercially produced fruits are likely to be polluted with pesticides.",
	    "Hylocereus sp. or The Dragon Fruit or Pitaya or Pitahaya\n\nFamily:  Cactaceae; Native to Mexico, Central America and South America\nA sheep in wolf's clothing?  This scary looking fruit of the Hylocereus species has a taste that�s been described as being very bland like a melon or kiwi with a mild sweetness.  Its black crunchy seeds are eaten together with the flesh but don't eat the skin. The skins of most commercially produced fruits are likely to be polluted with pesticides.",
	    "Theobroma cacao or The Chocolate Tree\n\nFamily:  Malvaceae; Native to the American Tropics\nHeaven �s cent�Theobroma comes from the Greek, meaning �food of the gods.�  The seeds or beans  of the cacao, said �ca kay oh�, tree are used to make cocoa powder and chocolate.  Don�t try planting this tree in your yard, however.   The only state that will support its growth is Hawaii.",
	    "Clusia major - Pitch Apple or Autograph Tree \n\nFamily: Clusiaceae; Native to the Caribbean;  Panama\n\nGooey business!!!  This fruit of Caribbean beauty produces a black resinous material which was used in the olden days to tar ships and thus waterproof them.  Its upper leaf tissues registers �writing� thus giving it its nickname, Autograph Tree.",
	    "Clusia major - Pitch Apple or Autograph Tree \n\nFamily: Clusiaceae; Native to the Caribbean;  Panama\n\nGooey business!!!  This fruit of Caribbean beauty produces a black resinous material which was used in the olden days to tar ships and thus waterproof them.  Its upper leaf tissues registers �writing� thus giving it its nickname, Autograph Tree.",
	    "Manikara zapota or  Sapodilla or The Chewing Gum Tree\n\nFamily:  Sapotaceae; Native to Central America\n\nA sticky situation  The gummy latex sap of the bark of this tree native to South America was first used by the Mayans and the Aztecs to make chewing gum.  The Wrigley Company was a prominent user of it until the 1960 �s when it was replaced by a synthetic substitute.",
	    "Manikara zapota or  Sapodilla or The Chewing Gum Tree\n\nFamily:  Sapotaceae; Native to Central America\n\nA sticky situation  The gummy latex sap of the bark of this tree native to South America was first used by the Mayans and the Aztecs to make chewing gum.  The Wrigley Company was a prominent user of it until the 1960 �s when it was replaced by a synthetic substitute.",
	    "Manikara zapota or  Sapodilla or The Chewing Gum Tree\n\nFamily:  Sapotaceae; Native to Central America\n\nA sticky situation  The gummy latex sap of the bark of this tree native to South America was first used by the Mayans and the Aztecs to make chewing gum.  The Wrigley Company was a prominent user of it until the 1960 �s when it was replaced by a synthetic substitute.",
	    "Mimosa pudica or the Sensitive Plant\n\nFamily: Leguminosae; Native to Brazil\n\nIt �s no shrinking violet but a gentle touch or a soft breeze is all that is needed to cause the rapid drooping of the leaves of the Sensitive Plant.  Why not touch for yourself and be amazed at the rapid seismonastic movement that results?",
	    "Mimosa pudica or the Sensitive Plant\n\nFamily: Leguminosae; Native to Brazil\n\nIt �s no shrinking violet but a gentle touch or a soft breeze is all that is needed to cause the rapid drooping of the leaves of the Sensitive Plant.  Why not touch for yourself and be amazed at the rapid seismonastic movement that results?" };

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_search);

	display = (TextView) findViewById(R.id.textView1);

	Spinner spin = (Spinner) findViewById(R.id.spinner1);
	spin.setOnItemSelectedListener(this);

	ArrayAdapter aa = new ArrayAdapter(this,
		android.R.layout.simple_spinner_item, search_words);

	aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	spin.setAdapter(aa);
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
	    long arg3) {
	// TODO Auto-generated method stub
	display.setText(search_values[arg2]);
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
	// TODO Auto-generated method stub
	display.setText("");
    }

}
