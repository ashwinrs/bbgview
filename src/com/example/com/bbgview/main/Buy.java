package com.example.com.bbgview.main;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Buy extends ListActivity {

    String[] Buy_Elements = { "Silver Bismarkia - $50", "Teddy Bear Palm - $50",
	    "cashew tree - $50", "Lychee Fruit - $40", "Dragon Fruit - $100", "Pitaya - $100",
	    "Chocolate Tree - $100", "Pitch Apple - $50", "Clusia Major - $50", "Manikara zapota - $100",
	    "Sapodilla - $50", "The Chewing Gum Tree - $100", "Mimosa pudica - $100",
	    "Sensitive Plant - $50" };

    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_buy,
		Buy_Elements));

	ListView listView = getListView();
	listView.setTextFilterEnabled(true);

	listView.setOnItemClickListener(new OnItemClickListener() {
	    public void onItemClick(AdapterView<?> parent, View view,
		    int position, long id) {
		// When clicked, show a toast with the TextView text
		Toast.makeText(getApplicationContext(),
			((TextView) view).getText()+". Has been added to your cart.", Toast.LENGTH_SHORT).show();
	    }
	});

    }

}
